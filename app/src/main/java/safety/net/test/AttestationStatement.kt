package safety.net.test

import com.google.api.client.json.webtoken.JsonWebToken
import com.google.api.client.util.Key

/**
 * Created by Ruslan Arslanov on 07/08/2018.
 */
class AttestationStatement : JsonWebToken.Payload() {

    /**
     * Embedded nonce sent as part of the request.
     */
    @Key
    var nonce: String? = null

    /**
     * Timestamp of the request.
     */
    @Key
    var timestampMs: Long = 0

    /**
     * Package name of the APK that submitted this request.
     */
    @Key
    var apkPackageName: String? = null

    /**
     * Digest of certificate of the APK that submitted this request.
     */
    @Key
    var apkCertificateDigestSha256: Array<String>? = null

    /**
     * Digest of the APK that submitted this request.
     */
    @Key
    var apkDigestSha256: String? = null

    /**
     * The device passed CTS and matches a known profile.
     */
    @Key
    var ctsProfileMatch: Boolean = false

    /**
     * The device has passed a basic integrity test, but the CTS profile could not be verified.
     */
    @Key
    var basicIntegrity: Boolean = false

    fun getFullStatement(): String {
        return "Statement: \n" +
                "nonce: $nonce\n" +
                "timestamp: $timestampMs\n" +
                "appPackageName: $apkPackageName\n" +
                "apkCertificateDigestSha256: $apkCertificateString\n" +
                "apkDigestSha256: $apkDigestSha256\n" +
                "ctsProfileMatch: $ctsProfileMatch\n" +
                "basicIntegrity: $basicIntegrity\n"
    }

    val apkCertificateString: String
        get() = apkCertificateDigestSha256?.joinToString() ?: ""

}