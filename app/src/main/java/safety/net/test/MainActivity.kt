package safety.net.test

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Base64
import android.util.Log
import android.view.View
import com.google.android.gms.safetynet.SafetyNet
import com.google.android.gms.safetynet.SafetyNetApi
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.client.json.webtoken.JsonWebSignature
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    private var nonce: ByteArray = ByteArray(24)
    private var dateFormatter = SimpleDateFormat("dd MMMM yyyy, HH:mm:ss:S", Locale.getDefault())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        generateNonce()

        attestButton.setOnClickListener { attest() }
        generateButton.setOnClickListener { generateNonce() }
    }

    private fun handleSuccessfulResponse(response: SafetyNetApi.AttestationResponse) {
        attestButton.visibility = View.VISIBLE
        attestProgress.visibility = View.GONE

        val jws = JsonWebSignature.Parser(JacksonFactory.getDefaultInstance())
            .setPayloadClass(AttestationStatement::class.java)
            .parse(response.jwsResult)

        val statement = jws.payload as AttestationStatement
        Log.w("SafetyNet Response", statement.getFullStatement())

        displayAttestationResult(statement)
    }

    private fun handleErrorResponse(exc: Exception) {
        exc.printStackTrace()
    }

    private fun generateNonce() {
        Random().nextBytes(nonce)
        nonceText.text = Base64.encodeToString(nonce, Base64.DEFAULT)

        // Hide previous attestation result.
        attestationResult.visibility = View.GONE
    }

    private fun attest() {
        attestButton.visibility = View.GONE
        attestProgress.visibility = View.VISIBLE
        attestationResult.visibility = View.GONE

        val task = SafetyNet
            .getClient(this)
            .attest(nonce, BuildConfig.API_KEY)

        task.addOnSuccessListener(::handleSuccessfulResponse)
        task.addOnFailureListener(::handleErrorResponse)
    }

    private fun displayAttestationResult(attestationStatement: AttestationStatement) {
        attestationResult.visibility = View.VISIBLE

        val time = dateFormatter.format(Date(attestationStatement.timestampMs))
        val timeStampText = "${attestationStatement.timestampMs},\n$time"

        timestampResponse.text = timeStampText
        nonceResponse.text = attestationStatement.nonce
        apkPackageNameResponse.text = attestationStatement.apkPackageName
        apkDigestSha256Response.text = attestationStatement.apkDigestSha256
        basicIntegrityResponse.text = attestationStatement.basicIntegrity.toString()
        ctsProfileMatchResponse.text = attestationStatement.ctsProfileMatch.toString()
        apkCertificateDigestSha256Response.text = attestationStatement.apkCertificateString
    }

}
